
// #3 code
fetch("https://jsonplaceholder.typicode.com/todos", {

        method: "GET",
        headers: {
            "Content-Type": "application/json"
        },

    })

    .then((response) => response.json())
    .then((json) => console.log(json))


// #4 

fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => {
    json.map(todos => console.log(todos.title))
})


// #5

fetch("https://jsonplaceholder.typicode.com/todos/3", {

        method: "GET",

        headers: {
            "Content-Type": "application/json"
        },

    })

    .then((response) => response.json())
    .then((json) => console.log(json))

// #6

fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => {
    json.map(todos => console.log(todos.title, todos.completed))
})

// #7

fetch("https://jsonplaceholder.typicode.com/todos", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Created To Do List Item",
            completed: false,
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

// #8

fetch("https://jsonplaceholder.typicode.com/todos/9", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Updated To Do List Item",
            description: "To update my to do list with a different data structure.",
            status: "Pending",
            dateCompleted: "Pending",
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

// 9

fetch("https://jsonplaceholder.typicode.com/todos/200", {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Title",
            description: "Description",
            completed: "Status",
            dateCompleted: "Date Completed",
            userId: ""
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

// 10


fetch("https://jsonplaceholder.typicode.com/todos/12", {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Updated To Do List Item",
            description: "To update my to do list with a different data structure.",
            status: "Pending",
            dateCompleted: "Pending",
            userId: 1
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))
   
// 11

fetch("https://jsonplaceholder.typicode.com/todos/200", {
        method: "PATCH",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Updated status and add date",
            description: "status is completed",
            completed: "true",
            dateCompleted: "08-27-2022",
            userId: 11
        })
    })
    .then((response) => response.json())
    .then((json) => console.log(json))

// 12

fetch("https://jsonplaceholder.typicode.com/todos/15", {
        method: "DELETE",
    })
    .then((response) => response.json())
    .then((json) => console.log(json))